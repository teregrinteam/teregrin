
[TOC]

## About Teregrin

### What is it?

A Gradle plugin to facilitate wrapping a Gradle build script around the 
execution of [Terraform](https://www.terraform.io/) (abbreviated below to "TF").

### What does it do?

It does three things:

* document and codify the exact version of Terraform that the build depends on
* document and codify the invocation of init/plan/apply etc. so users don't 
have to remember the exact options that are needed   
* remove the need to be concerened with if or what version of Terraform is 
installed on the target build environment.

If you're already able to run gradle tasks on the target build environment
then there is no configuration/environment preparation to be done.

### How does it work?

Add tasks and methods to your Gradle script to download a specific version of 
Terraform for you and execute generic refresh/plan/apply lifecycle actions.
 
The point is that versions of the Terraform binary are uploaded to Bintray at: 
https://bintray.com/shorn/teregrin/terraform-binary

This plugin will then download and execute the TF binary of whatever version 
you specify in your build config, taking into account the platform that the 
build is being run on (Windows, Linux, etc.).

It does effectively the same thing as the gradle-wrapper does for Gradle: remove any 
uncertainty regarding what version of TF your build depends on, and remove the  
need to think about whether or not the correct version of TF is installed  
on your target execution environment.

### Is it production ready?

I would only advise using Teregrin (abbreviate below to "TG") as an 
example/template for managing your own infrastructure with a  
Terraform Gradle script wrapper.

There are many Terraform wrapper projects out there: 
https://www.google.com/search?q=terraform+wrapper

You can use Teregrin to get started with Terraform but if you're considering 
managing a serious set of infrastructure - I'd advise managing 
your own scripts until one of the choices starts to emerge as a clear winner.


### Pre-requisites

Make sure you have a recent JDK (8+) installed and have a JAVA_HOME set 
(in your execution environment, doesn't have to be global or anything).

### Usage

The [Kite](https://bitbucket.org/kite-team/) project is will eventually 
supply some tutorials and examples of using Teregrin.

At the moment, you can see a concrete example in the 
[kite-mailer](https://bitbucket.org/kite-team/kite-mailer) 
repo, which
is a full working example of using Teregrin to implement an entire AWS 
infrastructure to forward email using SES and Lambda.

## Understanding Teregrin

### Motivation

#### Why does it exist?

Teregrin (TG) was created while working on AWS infrastructure, with a focus on
managing the tfstate out of distributed version control (Git/Mercurial).
I needed it to be usable from both Windows and Linux, in a manual and
automated context.

TG at the time was pre-release and changing rapidly and I wanted my 
infrastructure project to keep up with those changes, but I didn't want to 
have to maintain a bunch of Terraform (TF) installations on various boxes - 
dev workstations, CI servers, etc.  

Additionally, at the time, TF didn't have then concept of specifying the 
version that should be used - nothing worse than diagnosing version mismatches. 

I didn't want to remember all the command arguments, I didn't want to be 
checking in authentication to source control (see the security note) and I 
didn't want to be creating my own cross-platform startup scripts.  

I started out committing TF binaries to my repo - but that quickly got out 
of control in terms of size and I didn't want to use any Git's "big files" 
solutions.

#### Why does it need the JVM?

The JVM is my hammer and it's always available where I'm working; I like 
Gradle, I like the gradle wrapper concept and I wanted to extend it to my TF 
usage.
Since I was managing my tfstate via source control, Gradle
seemed a no-brainer to me.  The JDK is a very easy pre-requisite for me (no
need to even install it if you don't want, just copy it and set your JAVA_HOME).

Gradle and TF have strong support in my IDE of choice (IDEA), so that works
well for me too.


### Security 

Note that though TG is designed to store state in the version control
repo, it does nothing about what secrets TF might store in the state file.
And Terraform **does** store secrets in the state file, depending on the 
resource.
The big one for me is the RDS database password ends up in the state file, but
so do TLS keys, SSH keys (if created by TF), etc.  I've thought about 
implementing a masking feature for the state file, but honestly - storing state
in the version control repo really is a bit mickey-mouse.  If 
security is that big a thing for you, you should use a properly secured 
state-storage mechanism (even S3 with restricted privileges is probably better
than storing secrets in your version control system).

This is actually a huge downside of managing TF state out of a repo - I am very
much against storing any kind of secret in source (encrypted or not).  
In the future, I'm thinking about managing my RDS resources 
outside of TF.  The TF team have expressed opposition to building features to 
allow people to manage this - they want you to put your state somewhere 
designed to store secrets (preferably their paid solution :P )


### Downsides

TG is focused on managing state out of a shared repo (Git) - no thought
has gone into how it might work with storing state on Atlas, S3, etc.  
With appropriate segmentation
(different gradle projects, configuration blocks, manual coordination to
enforce serial access to minimise conflicts) - I think you could easily manage
an infrastructure made of somewhere between 10 - 200 machines.  Above that, I
think you'd need to start considering a different approach (I've toyed with 
the idea of doing my own coordination enforcement in before/after hooks in TG). 

TG was written for an AWS environment.  I've not done any testing at all outside
of that context.

At time of writing, I use Teregrin to manage a split-account setup  
(different development, production and reporting AWS accounts).  
I manage them as separate Gradle sub-projects within one high-level project, 
each account sub-project containing it's own separate TF state file.
The development account has about 6000 lines of TF files managing about 500 
resources.  At this point, it takes about a minute to do a full 
refresh/plan cycle.  Taking a minute or two to do a full refresh is starting 
to annoy me though, I'm thinking these projects need to take advantage of TF
modularisation (once piece of advice I'd give is to use modules to separate
applications from core infrastructure).


### Testing

It's only been tested with AWS.  There are some functional tests (that require
an AWS account), but they are very light-on at the moment.

### Support / Dealing with TG shortcomings

If you need more platforms or a new (or old) TF version uploaded, submit
an issue.

Submit feature requests or bug reports through the Bitbucket issue tracker.

### Terraform version binaries

New versions of the TF binaries are published manually, when I need them or 
when someone asks.  
Pull Requests for a new TF binary version will be accepted as soon as I 
know about them.


## Development

See [/doc/development.md](/doc/development.md)

[1]: https://bitbucket.org/shorn/kite
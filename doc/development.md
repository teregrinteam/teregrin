
[TOC]

### Publishing a new TF binary version 
 
The TF binaries are managed in the terraform-binary sub-project.
 
New TF version binaries are published to Bintray by using the [Bitbucket 
pipelines functionality](bitbucket-pipelines.yml), but runnin the 
binary_release custom pipeline.  

* on master branch
* edit the [tfVersion](terraform-binary/build.gradle) property in the source
* commit and push back to Bitbucket
* in the Bitbucket UI, navigate to the commit, then run the "binary_relase"
pipeline.

You can also do the above without checking out to your local machine at all - 
just edit the build.gradle file with Bitbucket's online editor and commit, then
run the pipeline.
 

### IDE configuration

See [/doc/idea.md](idea.md) for IDEA stuff.  
You're on your own for other IDEs.

### Contributing

If you haven't talked to the author first - a fork would be better approach 
than an unsolicited pull request.

### Continuous Integration

Commits on any branch other than binary_release will run `gradlew build`.
There aren't currently many unit tests.


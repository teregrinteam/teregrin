## IDEA 

Notes about setting up IDEA for Teregrin development

There's no ide integration plugins used in the gradle script at the moment.
I'm experimenting with IDEA's direct gradle import.

### Importing into IDEA

* make sure you've done a gradlew build before trying to import
* just "open" the build.gradle file (not the root directory, use "File/Open...", 
  not "import")
* the test-project is not a sub-project of the build, it's a completely 
standalone build.  Import it into IDEA by "attaching" the gradle project (
click the "+" icon in the gradle window then  select the build.gradle file.)

### Configuring / using IDEA

* launch gradle tasks by using the gradle window (don't try to Shift-F10 tasks)
* don't put auth details (bintray or terraform creds) in IDEA, use 
~/.gradle/gradle.properties
* IDEA doesn't pick up the Gradle source code now that we're using 
java-gradle-plugin
    * to solve, download the gradle-all distro jar yourself and "attach source" 
* if you want to run TestKit tests from the IDE, need to set these VM params:
    * -DTgTest.testProjectDir=<path>\\teregrin-gen\\test-project-dir -DTgTest.printOutput=true

### Problems


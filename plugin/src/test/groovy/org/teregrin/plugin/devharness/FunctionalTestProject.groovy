package org.teregrin.plugin.devharness

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner

/**
 * This stuff is deprecated now that I have proper GradleRunner working on the 
 * proper plugin under test.  
 * The plan is to refator these tests as proper unit tests and remove this 
 * (I will keep the test-project, but it's meant for proper functional testing 
 * on a published version of TG - no need to run it from the codebase like this.
 */
class FunctionalTestProject extends GroovyTestCase {
  // this points the tests at <root>/plugin/test-project/build.gradle
  File testProjectDir = new File("test-project")

  boolean printOutput = Boolean.getBoolean("FunctionalTest.printOutput")

  private void printOutput(BuildResult result, boolean override = false) {
    if( override || printOutput ){
      println "$name output..."
      println result.output
    }
    else {
      println "$name not printing functional test BuildResult"
    }
    
  }
  
  GradleRunner createGradleRunner(){
    return GradleRunner.create().
      withGradleVersion("3.3").
      withPluginClasspath()
  }

  void testTasksTask() {
    println "functional test with: `${testProjectDir.absolutePath}` ..."

    BuildResult result = createGradleRunner().
      withProjectDir(testProjectDir).
      withArguments("tasks").
      build(); 

    printOutput(result)
  }

  void testTgVersionTask() {
    println "functional test with: `${testProjectDir.absolutePath}` ..."

    BuildResult result = createGradleRunner().
//      withPluginClasspath(pluginClasspath).
//      withPluginClasspath([new File("C:\\1rslcare\\teregrin\\teregrin-gen\\build\\teregrin-plugin\\classes\\main")]).
//      withPluginClasspath([new File("C\\:/1rslcare/teregrin/teregrin-gen/build/teregrin-plugin/classes/main")]).
      withProjectDir(testProjectDir).
      withArguments("printTeregrinVersion").
      build();

    printOutput(result)

    assertTrue result.output.contains("Teregrin vnull")
  }

  void testTgDevPlanTask() {
    println "functional test with: `${testProjectDir.absolutePath}` ..."

    BuildResult result = createGradleRunner().
      withProjectDir(testProjectDir).
      withArguments(
        "devPlan", "-s",
        "-DtgTestDevAccessKey=xxx", "-DtgTestDevSecretKey=yyy").
      build();

    printOutput(result)

    assertTrue result.output.contains("InvalidClientTokenId:")
  }

}

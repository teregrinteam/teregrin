package org.teregrin.plugin.testkit

import org.gradle.testkit.runner.BuildResult

class TgPlanTest extends AbstractTgGradleRunnerTest {

  void testTgPlanTask() {
    
    writeTgBuildFile(null, null)
    
    
    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "devPlan").
      build()

    printOutput(result)

    assertTrue result.output.contains("running plan\n in directory")
    assertTrue result.output.contains("No changes. Infrastructure is up-to-date.")
  }
  
  void testTgPlanWithSameRunAndSrcDir() {

    File buildFile = new File(testProjectDir, "build.gradle")
    buildFile << """
      import org.teregrin.plugin.task.*
      
      plugins {
        id 'org.teregrin.plugin.teregrin-plugin'
      }
      
      teregrin {
        terraformVersion = '$tfVersion'
        tgDebug = true
        conf {
          dev {
            group = "teregrin.dev"
            runDirectory = file('tfSrc')
            environment << [
              'AWS_ACCESS_KEY_ID': 'xxx',
              'AWS_SECRET_ACCESS_KEY': 'xxx'
            ]
          }
        }
      }
      
    """

    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "devPlan").
      build()

    printOutput(result)

    assertTrue result.output.contains("running plan\n in directory")
//    processing configuration: dev running in  for tfSrc in null
    assertTrue result.output.contains("No changes. Infrastructure is up-to-date.")
  }

}


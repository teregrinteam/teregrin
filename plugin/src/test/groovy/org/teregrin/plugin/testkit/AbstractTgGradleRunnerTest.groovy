package org.teregrin.plugin.testkit

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.rules.TemporaryFolder

abstract class AbstractTgGradleRunnerTest extends GroovyTestCase {
  static List<String> standardArgs = [
    "--stacktrace",
//    "--debug"
  ]

  String tfVersion = "0.8.5"

  // setup stuff
  File testProjectDir
  File tfSrcDir
  File tfRunDir
  GradleRunner gradleRunner

  String testProjectDirName = System.getProperty("TgTest.testProjectDir", null)
  
  boolean printOutput = Boolean.getBoolean("TgTest.printOutput")
  boolean deleteSrcFiles = Boolean.getBoolean("TgTest.deleteSrcFiles")
  boolean useRandomNames = Boolean.getBoolean("TgTest.useRandomNames")

  @Override
  protected void setUp() throws Exception{
    super.setUp()

    assert testProjectDirName : 
      "you must specify a place to put the generated source files"
    
    def projectFilesParentDir = new File(testProjectDirName)
    projectFilesParentDir.mkdirs()
    
    if( useRandomNames ){
      TemporaryFolder tempDir = new TemporaryFolder(projectFilesParentDir)
      tempDir.create()
      testProjectDir = tempDir.root
    }
    else {
      testProjectDir = new File(projectFilesParentDir, "${testClassName}_$name")
    }

    testProjectDir.deleteDir()
    testProjectDir.mkdirs()
    
    
    println "$name functional test src file '${testProjectDir.absolutePath}' line: 0 ..."

    if( deleteSrcFiles ){
      testProjectDir.deleteOnExit()
    }
    
    tfSrcDir = new File(testProjectDir, "tfSrc")
    tfSrcDir.mkdirs()
    tfRunDir = new File(testProjectDir, "tfRun")
    tfRunDir.mkdirs()
    // so that there's always something for TF to read
    new File(tfSrcDir, "empty.tf").write " "
    
    gradleRunner = GradleRunner.create().
      withGradleVersion("3.3").
      withPluginClasspath()
  }

  @Override
  void tearDown(){
    if( deleteSrcFiles ){
      testProjectDir.delete()
    }
    super.tearDown()
  }

  void printOutput(BuildResult result, boolean override = false) {
    if( override || printOutput ){
      println "$name output..."
      println result.output
    }
    else {
      println "$name not printing functional test BuildResult"
    }
    
  }

  String constructTgBuildFile(
    String tgBlockContent = null, 
    String taskContent = null)
  {
    return """
      import org.teregrin.plugin.task.*
      
      plugins {
        id 'org.teregrin.plugin.teregrin-plugin'
      }
      
      teregrin {
        terraformVersion = '$tfVersion'
        tgDebug = true
        conf {
          dev {
            group = "teregrin.dev"
            runDirectory = file('tfRun')
            tfSrcDirectory = file('tfSrc')
            environment << [
              'AWS_ACCESS_KEY_ID': 'xxx',
              'AWS_SECRET_ACCESS_KEY': 'xxx'
            ]
          }
        }
        ${tgBlockContent ?: ""}
      }
      
      ${taskContent ?: ""}
    """
  }
  
  String writeTgBuildFile(String tgBlockContent, String taskContent){
    File buildFile = new File(testProjectDir, "build.gradle")
    buildFile << constructTgBuildFile(tgBlockContent, taskContent)
    
  }
  
}

package org.teregrin.plugin.testkit

import org.gradle.testkit.runner.BuildResult

class TgRemoteTest extends AbstractTgGradleRunnerTest {

  /**
   * This test is about using "multi-part" commands.
   */
  void testTgRemoteConfig() {

    File buildFile = new File(testProjectDir, "build.gradle")
    buildFile << remoteTestBuildFile()

    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "devRemoteConfig").
      build()

    printOutput(result)

    assertTrue result.output.contains("combined standard task options: [remote, config, -no-color")
    assertTrue result.output.contains("missing 'path' configuration")
  }

  void testTgRemotePlan() {

    File buildFile = new File(testProjectDir, "build.gradle")
    buildFile << remoteTestBuildFile()

    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "devPlan").
      build()

    printOutput(result)

    // when we're in a "remote state" situation we want there to be no state
    // on the command line
    assertTrue !result.output.contains(" -state=")
  }

  private GString remoteTestBuildFile() {
    """
      import org.teregrin.plugin.task.*
      
      plugins {
        id 'org.teregrin.plugin.teregrin-plugin'
      }
      
      teregrin {
        terraformVersion = '$tfVersion'
        tgDebug = true
        conf {
          dev {
            group = "teregrin.dev"
            runDirectory = file('tfRun')
            tfSrcDirectory = file('tfSrc')
            environment << [
              'AWS_ACCESS_KEY_ID': 'xxx',
              'AWS_SECRET_ACCESS_KEY': 'xxx'
            ]
          }
        }
      }
      
      task devRemoteConfig(type: TerraformTask) {
        conf = teregrin.conf.dev
        command = "remote config"
        noInput = false
        noColor = true
        tfState = null
        commandArgs = [
          "-backend=local"
        ]
      }

    """
  }

}


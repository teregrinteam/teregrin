package org.teregrin.plugin.testkit

import org.gradle.testkit.runner.BuildResult

class TgValidateTest extends AbstractTgGradleRunnerTest {

  void testTgValidateTask() {

    writeTgBuildFile(null, null)
    
    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "devValidate").
      build()

    printOutput(result)

    assertTrue( 
      "unexpected output: [${result.output}]",
      result.output.contains("running validate\n in directory") )
  }

  void testTgValidateTaskFailure() {
    
    writeTgBuildFile(null, null)
     
    // the value here ("blah") is invalid, should be in quotes
    new File(tfSrcDir, "bad_format_value.tf") << """
      provider "aws" {
        region = blah
      }
    """
    
    
    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "devValidate").
      build()

    printOutput(result)
    
    println result.output
    
    // if this is the only test run, it will output this, but if it runs after 
    // other tests, you won't see this
    assertTrue result.output.contains("running validate\n in directory")
    
    assertTrue result.output.contains("Error loading files")
    assertTrue result.output.contains("bad_format_value.tf")
    assertTrue result.output.contains("Unknown token: 3:18 IDENT blah")
  }
  
}


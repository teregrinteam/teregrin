package org.teregrin.plugin.testkit

import org.gradle.testkit.runner.BuildResult

class TgVersionTest extends AbstractTgGradleRunnerTest {

  void testTgVersionTask() {
    super.writeTgBuildFile("", "")
    
    BuildResult result = gradleRunner.
      withProjectDir(testProjectDir).
      withArguments(standardArgs + "printTeregrinVersion").
      build()

    printOutput(result)

    assertTrue result.output.contains("Teregrin vnull")
  }
  
}


// this is just a place to work out my plans in progress, this is not doco
ext {
}

teregrin {
  boolean debug = false
  boolean forceUnzip = false
  String terraformVersion = null

  conf {
    dev {
      accessKey = System.getProperty("awsDevAccessKey")
      secretKey = System.getProperty("awsDevSecretKey")
      directory = 'src/main/terraform/devPlatform'
    }

    prd {
      accessKey = System.getProperty("awsPrdAccessKey")
      secretKey = System.getProperty("awsPrdSecretKey")
      directory = 'src/main/terraform/devPlatform'

    }
  }
}
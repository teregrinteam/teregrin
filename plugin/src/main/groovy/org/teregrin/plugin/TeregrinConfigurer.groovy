package org.teregrin.plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.teregrin.plugin.command.JavaCommand
import org.teregrin.plugin.command.TerraformCommand
import org.teregrin.plugin.task.GeneralTeregrinTask
import org.teregrin.plugin.task.TerraformTaintTask
import org.teregrin.plugin.task.TerraformTask
import org.teregrin.plugin.util.OsUtil

class TeregrinConfigurer {

  // these are passed in during construction
  Project project
  TeregrinPluginExtension teregrin
  Set<TeregrinConfigurationContainer> conf

  // used by tasks
  JavaCommand java
  TerraformCommand terraform

  private Configuration tfBinaryConfig
  private Dependency tfBinaryDependency

//  TeregrinLogger log = new TeregrinLogger(config: this)

  /**
   * I think this is what forces the actual download if it's not cached.
   */
  File resolveBinaryZipFile(){
    return tfBinaryConfig.fileCollection(tfBinaryDependency).singleFile
  }

  /**
   * Configures gradle to be able to resolve the binary zip file
   * and configures the TerraformCommand that will actually download and
   * extract the zip file if a task that uses tf command is used.
   */
  void configureTfBinaryDependency() {
    project.repositories {
      maven {
        url teregrin.artifactUrl
      }
    }


    tfBinaryConfig = project.configurations.create("teregrinTerraformBinary") {
      visible = false
      transitive = false
      extendsFrom = []
    }

    assert teregrin.terraformVersion:
      "teregrin.terraformVersion must contain the desired version of Terrafrom"
    def notation = [
      group     : 'teregrin',
      name      : 'terraform-binary',
      version   : teregrin.terraformVersion,
      classifier: OsUtil.getTfOsClassifer(),
      ext       : 'zip' ]
    tfBinaryDependency = project.dependencies.add(tfBinaryConfig.name, notation)

    terraform = new TerraformCommand(config: this, log: teregrin.log)
  }

  /**
   * Adds a printTeregrinVersion task that will print the version of the TG
   * plugin and the version of Terraform (from TF itself, so this will download
   * TF if necessary).
   */
  void addGeneralTasks() {
    project.tasks.create('printTeregrinVersion', GeneralTeregrinTask) {
      description = "print the version of the Teregrin plugin and Terraform"
      outputs.upToDateWhen { false }
      doLast {
        // depends on impl version being set correctly in the Manifest file,
        // see jar task in plugin gradle file
        teregrin.log.output "Teregrin v" +
          getClass().getPackage().getImplementationVersion()

        def result = terraform.execCommand {
          workingDir = terraform.unzipDir
          args "version"
        }

        // NOTE:STO I think this is not needed anymore?, shouldn't the output
        // be being echoed to the console by TfCommand itself?
        result.printResult(teregrin.log)
      }
    }

  }

  /**
   * Adds a bunch of configuration specific tasks (plan, apply, taint, etc.)
   * <p/>
   * Intentionally not adding default "destroy" tasks, too scary.
   * <p/>
   * Intentionally not adding every possible command, to avoid cluttering up
   * the build task list.
   * <p/>
   * Should probably use task rules for the less common
   * TF tasks (or possibly all of them really, more than a couple of configs is
   * going to result in a lot of tasks).
   * But task rules are not common and they confuse people / IDEA.
   * Could do both?  Config option to choose style, or possibly split into two
   * separate convention plugins?
   * <p/>
   */
  void addConfigurationTasks(){
    teregrin.log.debug "processing `${conf.size()}` configurations" +
      " on project `${project.name}`..."

    conf.each{ TeregrinConfigurationContainer iConf ->
      assert iConf.runDirectory : "no directory set"
      def relativeRunDirname = iConf.runDirectory.absolutePath -
        project.rootProject.projectDir.absolutePath

      def relativeTfSrcDirname
      if( iConf.tfSrcDirectory ){
        relativeTfSrcDirname = iConf.tfSrcDirectory.absolutePath -
          project.rootProject.projectDir.absolutePath
      }
      else {
        relativeRunDirname = ""
      }

      teregrin.log.debug "processing configuration: ${iConf.name}" +
        " running in ${relativeRunDirname}" +
        " for tfSrc in ${relativeTfSrcDirname}"


      project.tasks.create(name: "${iConf.name}Plan", type: TerraformTask) {
        delegate.conf = iConf
        delegate.description = "run `Terraform plan`" +
          " from `${relativeRunDirname}`" +
          " for src `${relativeTfSrcDirname}`"
        delegate.command = "plan"
        delegate.mustRunAfter "${iConf.name}Refresh"
      }
      project.tasks.create(name: "${iConf.name}Validate", type: TerraformTask) {
        delegate.conf = iConf
        delegate.description = "run `Terraform validate`" +
          " from `${relativeRunDirname}`" +
          " for src `${relativeTfSrcDirname}`"
        delegate.command = "validate"
        // these two options below don't work with validate command
        delegate.tfState = null
        delegate.noInput = false
      }
      project.tasks.create(name:"${iConf.name}Apply", type:TerraformTask){
        delegate.conf = iConf
        delegate.description = "run `Terraform apply`" +
          " from `${relativeRunDirname}`" +
          " for src `${relativeTfSrcDirname}`"

        delegate.command = "apply"
        if( teregrin.autoApproveApply ){
          delegate.commandArgs << "-auto-approve"
        }
      }
      project.tasks.create(name: "${iConf.name}Refresh", type: TerraformTask){
        delegate.conf = iConf
        delegate.description = "run `Terraform refresh`" +
          " from `${relativeRunDirname}`" +
          " for src `${relativeTfSrcDirname}`"

        delegate.command = "refresh"
      }
      project.tasks.create(
        name: "${iConf.name}Taint",
        type: TerraformTaintTask
      ) {
        delegate.conf = iConf
        delegate.description = "run `Terraform taint`" +
          " from `${relativeRunDirname}`" +
          " for src `${relativeTfSrcDirname}`"
        delegate.command = "taint"
      }
    }

  }

  /**
   * This configures all the plugin-created tasks, but will also hookup any
   * tasks of type TerraformTask that the user has defined in their build.
   */
  void configureTerraformTasks() {
    project.getTasks().withType(TerraformTask){ TerraformTask iTask ->
      iTask.terraform = iTask.terraform ?: this.terraform

      // another attempt to work around the intermittent "no ouput printed"
      // problem, I'm wondering if the task is just not executing
      iTask.outputs.upToDateWhen { false }

      if( teregrin.tfDebug ){
        iTask.tfDebug = true
      }
      else {
        // leave the task alone, if it's set to log, it will log
      }
    }
  }

  void configureJava() {
    java = new JavaCommand(config: this, log: teregrin.log)
  }
}

package org.teregrin.plugin.util

import java.nio.channels.FileChannel
import java.nio.file.attribute.FileTime
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

class ZipUtil {

  /**
   * Sets the creation, access and modified dates on every file in the archive
   * to epoch date.
   * <p/>
   * Call with (filepath) if you want to stomp over the old file with the new
   * content.
   * <p/>
   * Call with (filepath, false, false) if you want the new content to be
   * sitting in the same directory but with a name with ".normalised.zip"
   * appended.
   * <p/>
   * Really nasty, awful code - especially around resource handling and cleanup.
   * Needs rework.
   *
   * @param overwrite set to false if you don't want to overwrite the original file
   * @param cleanupTempFile set to false if you want to leave the normalised
   *        content in a file named "<filepath>.normalised.zip"
   */
  public static normaliseZipDates(
    String filepath,
    boolean overwrite = true,
    boolean cleanupTempFile = true
  ) {
    FileTime epoch = FileTime.fromMillis(0);
    String tempFilepath = filepath + ".normalised.zip";
    ZipInputStream zipInput = new ZipInputStream(new FileInputStream(filepath));
    ZipOutputStream zipOutput =
      new ZipOutputStream(new FileOutputStream(tempFilepath));
    byte[] buffer = new byte[8192];
    try {
      for(
        ZipEntry z = zipInput.getNextEntry();
        z != null;
        z = zipInput.getNextEntry()
      ){
        zipOutput.putNextEntry(
          z.setCreationTime(epoch).
            setLastAccessTime(epoch).
            setLastModifiedTime(epoch));

        int n;
        while( (n = zipInput.read(buffer, 0, buffer.length)) > 0 ){
          zipOutput.write(buffer, 0, n);
        }
      }
    }
    finally {
      zipInput.close();
      zipOutput.closeEntry();
      zipOutput.close();
    }

    if( overwrite ){
      FileChannel src = new FileInputStream(tempFilepath).getChannel();
      FileChannel dest = new FileOutputStream(filepath).getChannel();
      try {
        dest.transferFrom(src, 0, src.size());
      }
      finally {
        src.close()
        dest.close()
      }
    }

    if( cleanupTempFile ){
      new File(tempFilepath).delete()
    }

  }
}

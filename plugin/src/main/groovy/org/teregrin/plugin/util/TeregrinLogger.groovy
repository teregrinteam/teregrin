package org.teregrin.plugin.util

import org.gradle.api.Project
import org.teregrin.plugin.TeregrinConfigurer

class TeregrinLogger {
  TeregrinConfigurer config
  Project project

  void output(String s) {
    loud(s)
  }

  void debug(String s) {
    if (config.teregrin.tgDebug) {
      loud(s)
    }
    else {
      quiet(s)
    }
  }
  
  private void quiet(String s){
    project.logger.debug s 
  }
  
  // If i use "println" or "logger.info", the information sometimes doesn't 
  // actually get printed to the console which causes the unit tests to fail.
  private void loud(String s){
    project.logger.warn s
  }

}

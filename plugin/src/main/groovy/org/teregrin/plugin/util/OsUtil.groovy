package org.teregrin.plugin.util

import org.apache.tools.ant.taskdefs.condition.Os

class OsUtil {

  /** Deals with .exe for windows properly */
  static String getTfOsBinaryName() {
    if (Os.isFamily(Os.FAMILY_WINDOWS)) {
      return "terraform.exe"
    } else {
      return "terraform"
    }
  }

  /**
   * Maps the current OS to Terraform's download archive name format
   * Uses the Ant "Os" class to figure it out.
   * <p/>
   * I wanted to use the google osdetector plugin, but then I'd have to
   * figure out how to add repos from within the plugin.
   * <br/>
   * Also osdetecter was actually not working even from a
   * normal build at the time, giving weird transitive dependency
   * errors, had to exclude exclude the 'sisu-guice' module to even make it
   * work at all, at which point I decided I didn't really want to depend on
   * some flakey internal product of googles.
   */
  static String getTfOsClassifer() {
    if (Os.isFamily(Os.FAMILY_WINDOWS)) {
      // supposedly, if you're running a 32bit JVM on 64bit windows, this
      // will report i386, feel free to fix if it's a problem for you.
      // see http://stackoverflow.com/questions/1856565/how-do-you-determine-32-or-64-bit-architecture-of-windows-using-java/2269242#2269242
      if (Os.isArch("amd64")) {
        return "windows_amd64"
      }
    } else if (Os.isFamily(Os.FAMILY_MAC)) {
      if (Os.isArch("amd64")) {
        return "darwin_amd64"
      }
    } else if (Os.isFamily(Os.FAMILY_UNIX)) {
      if (Os.isArch("amd64")) {
        return "linux_amd64"
      }
    }

    throw new UnsupportedOperationException(
      "Teregrin doesn't currently do:" +
        " " + System.getProperty("os.name") +
        " " + System.getProperty("os.arch") +
        " " + System.getProperty("os.version"))
  }
}

package org.teregrin.plugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.teregrin.plugin.util.TeregrinLogger

/**
 * The core plugin extension, available in the build as "project.teregrin"
 */
class TeregrinPluginExtension{
  
  boolean tgDebug = false

  /** This will propagate to tasks, if this is true, all tasks will do
   * TF logging (regardless of their individual settings.
   * If false, then no tasks will do TF logging by default, but you can set
   * the tgDebug property on individual tasks to turn it on for just that task.
   * There's no conf-level property yet, just didn't make sense to me at the
   * time, feel free to add if it makes sense.
   */
  boolean tfDebug = false

  /** will force teregrin to re-unzip the archive if set to true */
  boolean forceUnzip = false

  /** specifies the version of TF that you want your project to use, see
   * the files available at {@link #artifactUrl} to check what versions are
   * available.  Submit a request to the plugin maintainer if you want a
   * specific version/architecture that is not currently available.
   */
  String terraformVersion = null

  /** At some point in the future, this will be used to call back into the
   * gradle project execute arbitrary gradle tasks/code.
   */
  String javaHome = null

  /** the ":terraform-binary" sibling sub-project publishes binaries at this
   * location in the standard maven format (I think, really I just frobbed
   * the directory structure/filename format until Gradle appeared to work.)
   */
  String artifactUrl = "https://bintray.com/artifact/download/shorn"

  boolean defaultNoColor = true 
    
  /** This property is for internal use of the plugin (really, it ought to be
   * hidden from the user) - it's part of the implementation of the "tgDebug"
   * option.
   * <p>
   * Sometimes I want logging specific to a plugin.  But it seems like the
   * only way to get debug info with Gradle is to switch all debugging on with
   * "-d" which will flood you with debugging from the entire gradle ecosystem.
   * This lets me toggle debugging info only for the Teregrin plugin.
   * There's got to be a better way I'm missing.  At least implementationally,
   * maybe I can toggle a proper logger class directly in code based on the
   * tgDebug setting?
   */
  TeregrinLogger log 
  
  /**
   * "debug" is now "tgDebug", this method will fast-fail projects that use
   * the old flag, with a helpful message
   */
  public setDebug(boolean value){
    throw new UnsupportedOperationException(
      "'debug' flag no longer supported: use 'tgDebug' for enabling " +
        "Teregrin logging instead, use 'tfDebug' to enable Terraform logging")
  }

  /** TF 0.11.0 onward is interactive by default - which will fail when run by
   * by TG because it's not interactive.
   * <p/>
   * This switch adds "-auto-approve" option to the apply task, which reverts
   * to the old behaviour.  The autoApproveApply option is added because
   * I'm assuming if you try to use this with older version of TF, it will
   * fail because older versions of TF won't understand the option.  Set this
   * option to false and hopefully the older TF version will work.
   * <p/>
   * Default value for this option is true because it is assumed users
   * will be using TG > 0.2.4 with TF > 0.11.0.
   * This option can be removed when assured noone is using TG with TF
   * versions < 0.11.
   */
  boolean autoApproveApply = true

}

/**
 * Intended for configuration of arbitrary numbers of "configurations", that
 * TG then uses to spray out some default tasks.  Available in the build as
 * "project.teregrin.conf" (list of of objects of this type.)
 */
class TeregrinConfigurationContainer {
  String name

  /**
   * Specifies the gradle group that all tasks for this configuration will use
   */
  String group

  /**
   * Specigies environment variables that you want to use for all tasks in a
   * conf block (originall intended for AWS authentication details.)
   */
  Map<String, String> environment = [:]

  /**
   * The directory that all tasks will run from.
   */
  File runDirectory

  /**
   * The directory that tasks will read the TF source from, if this is null
   * Terraform will look for the source in whatever directory it's run from.
   */
  File tfSrcDirectory

  /**
   * The file that stores the tfstate.
   */
  File tfState

  TeregrinConfigurationContainer(String name) {
    this.name = name
  }

}

/**
 * Adds a {@link TeregrinPluginExtension} extension point to the build under
 * name "teregrin".
 *
 * Adds some general usage tasks, see
 * {@link TeregrinConfigurer#addGeneralTasks()}.
 *
 * Adds some configuration specific tasks, see
 * {@link TeregrinConfigurer#addConfigurationTasks()}.
 *
 */
class TeregrinPlugin implements Plugin<Project> {

  Project project
  TeregrinPluginExtension teregrin
  TeregrinConfigurer configurer

  @Override
  public void apply(Project project) {
    this.project = project

    project.extensions.create("teregrin", TeregrinPluginExtension)
    teregrin = project.teregrin
    project.teregrin.extensions.conf =
      project.container(TeregrinConfigurationContainer)

    configurer = new TeregrinConfigurer(
      teregrin: teregrin,
      project: project,
      conf: project.teregrin.extensions.conf
    )
    teregrin.log = new TeregrinLogger(config: configurer, project: project)


    project.afterEvaluate {
      configurer.configureTfBinaryDependency()
      configurer.configureJava()

      configurer.addGeneralTasks()
      configurer.addConfigurationTasks()

      configurer.configureTerraformTasks()
    }


  }

}

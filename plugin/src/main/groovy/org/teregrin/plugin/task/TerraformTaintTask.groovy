package org.teregrin.plugin.task

import org.teregrin.plugin.command.TfExecResult

class TerraformTaintTask extends TerraformTask {
//  String resourceName

  void configureDefaultResourceName() {
    if (!commandArgs.empty) {
      return
    }
    String resourceNameProperty = "${this.name}Resource"
    log.debug "resource name not set, trying to get it from property: " +
      "`${resourceNameProperty}`"
    String resourceNameValue = project."${resourceNameProperty}"
    log.debug "configured resource name: " +
      "`${resourceNameProperty}`=`${resourceNameValue}`"
    commandArgs = [resourceNameValue]
  }

  @Override
  TfExecResult execTerraform() {
    configureDefaultResourceName()
    log.output "running taint against `${[commandArgs]}` in directory:" +
      " ${runDirectory.absolutePath} ..."
    return super.execTerraform()
  }


}

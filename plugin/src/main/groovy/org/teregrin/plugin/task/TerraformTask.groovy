package org.teregrin.plugin.task

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.teregrin.plugin.TeregrinConfigurationContainer
import org.teregrin.plugin.command.TerraformCommand
import org.teregrin.plugin.command.TfExecResult
import org.teregrin.plugin.util.TeregrinLogger

/**
 * TODOC: need to decide on, then document terminology:
 * args/options/tfArgs/commandArgs/flags/environment, etc.
 * <P>
 *
 * <UL>
 *   <LI>options are meant to be "global" terraform options - e.g. "-no-color"</LI>
 *   <LI>command is the actual TF command (not the executable) - e.g. "plan"</LI>
 *   <LI>commandArgs are arguments specific to the command being used</LI>
 *   <LI></LI>
 * </UL>
 * <P>
 *
 * <pre>[environment] terraform [command] [tfOptions] [commandArgs]</pre>
 * <pre>TF_LOG=TRUE terraform plan -no-color -destroy</pre>
 * <UL>
 *   <LI>"TF_LOG" is an environment variable</LI>
 *   <LI>"plan" is the command</LI>
 *   <LI>"-no-color" is a standard global TF option</LI>
 *   <LI>"-destroy" is a commandArg specific to the plan command</LI>
 *   <LI></LI>
 * </UL>
 */
class TerraformTask extends DefaultTask {
  
  /** "get" here is talking about the TF get command, not related to
   * groovy/java properties
   */
  public static final List<String> getTfOptions = ["-no-color"]

  TerraformCommand terraform
  TeregrinLogger log

  /** The directory that the command will be executed from.
   */
  File runDirectory

  /** The directory where the *.tf files sit
   */
  File tfSrcDir

  /* The TF command, like "plan" */
  String command

  /** arguments that are specific to the command, like "-destroy" for the
   * "plan" command.  Needs to be separate from tfArgs because TF has specific
   * requirements for the ordering of args.
   */
  List<String> commandArgs = []

  /** extra "global" TF options that the specific task might want to speecify
   * (in addition to any standard tfOptions that the plugin might be
   * applying to all tasks)
   */
  List<String> tfArgs = []

  /** Environment variables (TF_LOG, AWS authentication details, etc.) */
  Map<String, String> environment = [:]

  /** if the root-level teregrin-level property is true, this will get
   * overriden to true, see {@link org.teregrin.plugin.TeregrinConfigurer}
   */
  boolean tfDebug = false

  /** File to store the tfstate in, will default to standard TF location if not
   * set.
   */
  File tfState
  
  /** if true (default), standard ags will contain "-input=false" */
  Boolean noInput = true
  
  /** if true (null), standard args will contain whatever the plugin defines 
   * as the global default in the "teregrin.defaultNoColour" extenstion 
   * property. 
   */
  Boolean noColor

  /** Override this closure when full control of the command args is needed.
   * Do not return null (and empty is pretty pointless, unless you 
   * want to see a usage statement). 
   */
  Closure<Object[]> constructExecutionArgs = this.&constructStandardArgs

  TerraformTask() {
    group = "teregrin"
    assert project.teregrin.log
    this.log = project.teregrin.log

    outputs.upToDateWhen { false }
  }

  /*
  tfOptions should come from the task too, and then the standard options
  are only used if the user doesn't configure any optiosn on the task
  (or possibly via a flag).

  And maybe even the default standard/get options should be overridable up
  at the conf level?
   */
  Object[] constructStandardArgs() {
    List<String> args = []
    args.addAll command.split(" ")

    if( noColor != null ){
      if( noColor.booleanValue() ){
        args += "-no-color"
      }
    }
    else {
      if( project.teregrin.defaultNoColor ){
        args += "-no-color"
      }
    }
    
    if( noInput ){
      args += "-input=false"
    }

    if( tfState ){
      args += "-state=${tfState.getAbsolutePath()}"
    }
    
    args += tfArgs
    args += commandArgs

    // if it's to be used it should be the last thing on the command line
    if( tfSrcDir ){
      args += tfSrcDir.getAbsolutePath()
    }

    log.debug "combined standard task options: ${args}"
    return args as Object[]
  }

  Map<String, String> createStandardEnvironment() {
    Map<String, String> args = [:]
    args += this.environment
    if (tfDebug) {
      args["TF_LOG"] = "TRACE"
    }
    return args
  }

  TfExecResult execTerraform() {
    log.output "running ${command}\n" +
      " in directory: ${runDirectory.absolutePath}\n" +
      " against tfSrc: ${tfSrcDir?.absolutePath}"
    return terraform.execCommand {
      workingDir = runDirectory.absolutePath
      
      def builtArgs = constructExecutionArgs.call()
      if( !builtArgs ){
        log.output "constructed execution args is null, converted to empty"
        builtArgs = []
      }
      args builtArgs

      // must be += or won't be able to find temp directory, etc.
      delegate.environment += createStandardEnvironment()
    }
  }

  @TaskAction
  void taskAction() {
    // don't need to print results anymore, because the TfCommand now streams
    // the output asynchronously (need to add configurability to that?)
    TfExecResult result = execTerraform()
    this.extensions.add("execResult", result)
  }

  /**
   * "populate from conf", copies various properties of the given
   * configuration container.
   * It's done as a setter method like this so the DSL is nice for people
   * configuring tasks by hand, but wanting to reuse conf settins.
   */
  void setConf(TeregrinConfigurationContainer conf) {
    if( conf.group ){
      this.group = conf.group
    }
    this.runDirectory = conf.runDirectory

    // not sure about this logic, does it need to be more like tfState?
    if( conf.tfSrcDirectory ){
      this.tfSrcDir = conf.tfSrcDirectory
    }

    this.environment = conf.environment
    
    if( conf.tfState ){
      this.tfState = conf.tfState
    }

  }

}

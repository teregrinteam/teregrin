package org.teregrin.plugin.command

import org.apache.commons.io.output.TeeOutputStream
import org.gradle.process.ExecResult
import org.teregrin.plugin.TeregrinConfigurer
import org.teregrin.plugin.util.OsUtil
import org.teregrin.plugin.util.TeregrinLogger

class TfExecResult {
  int returnCode
  String stdOut
  String stdErr

  /**
   * Straight to stdout via println, need to parameterise
   */
  void printResult(TeregrinLogger log){
    log.output stdOut + "\n" +  stdErr
  }
}

class TerraformCommand {

  TeregrinConfigurer config
  TeregrinLogger log

  private File unzipDir
  private File tfCommand

  File getUnzipDir() {
    return unzipDir
  }

  /** if not already there will download and extract the TF binaries */
  void extractTerraform() {
    if( unzipDir ){
      // shortcut - this method's been called before on this run
      return
    }

    log.debug "resolving terraform binary for version" +
      " [${config?.teregrin?.terraformVersion}] ..."
    // I thought this was the line causing the download,
    // but actually, when I've been downloading new versions, there's a big
    // pause before I see the above debug line, leading me to think that the
    // resolve statement below isn't what actually does the downloading
    File file = config.resolveBinaryZipFile()
    log.debug "resolved to: $file.absolutePath"
    assert file.exists() && file.canRead()

    unzipDir = new File(file.absoluteFile.parent, "unzip")

    tfCommand = new File(unzipDir, OsUtil.tfOsBinaryName)
    if( tfCommand.exists() && tfCommand.canExecute() ){
      if( config.teregrin.forceUnzip ){
        log.debug "forceUnzip set to true"
      }
      else {
        log.debug "tf command file exists already, skipping the unzip"
        return
      }
    }
    else {
      log.debug "no working tf command file found, unzipping"
    }

    log.debug "unzipping terraform binaries"
    config.project.ant.unzip(
      src: file.absolutePath,
      dest: unzipDir.absolutePath,
      overwrite: "true" )

    log.debug "setting executable permissions for all files"
    config.project.ant.chmod(dir: unzipDir, perm: '+x', includes: "*")

    assert tfCommand.exists() && tfCommand.canExecute():
      "after unzipping, could not find executable: ${tfCommand.absolutePath}"
  }

  /**
   * compose the passed closure with ours that sets the streams,
   * then pass that to the exec() method to configure the execspec
   * then execute.
   * <p/>
   * Method needs to be changed to "stream" the output asynchronously,
   * so we can see what's going on instead of having to wait until the end
   * of the execution before spraying the output to the log.
   */
  TfExecResult execCommand(Closure c) {
    extractTerraform()

    def outCaptureStream = new ByteArrayOutputStream()
    def errCaptureStream = new ByteArrayOutputStream()
    def outTos = new TeeOutputStream(System.out, outCaptureStream)
    def errTos = new TeeOutputStream(System.err, errCaptureStream)

    // tyring to diagnose problem where we intermittently don't get any
    // output printed
    outCaptureStream.println("*** TF output starts here")

    ExecResult procResult = config.project.exec(c << {
      executable tfCommand.absolutePath
      ignoreExitValue = true
      standardOutput = outTos
      errorOutput = errTos
    })

    outCaptureStream.println("*** TF output stops here")

    // doco says it will flush first, but I don't see it in the code
    // so I added the flushes in the hopes it has something to do with the
    // intermittent "no ouput printed" problem
    outTos.flush()
    outTos.close()
    errTos.flush()
    errTos.close()

    TfExecResult result = new TfExecResult(
      returnCode: procResult.exitValue,
      stdOut: outCaptureStream.toString().trim(),
      stdErr: errCaptureStream.toString().trim() )

    if( procResult.exitValue != 0 ){
      log.output "execution returned: $procResult"
    }
    return result
  }
}

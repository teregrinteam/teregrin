package org.teregrin.plugin.command

import org.teregrin.plugin.TeregrinConfigurer
import org.teregrin.plugin.util.TeregrinLogger

/**
 * This class not finished yet.  It will be used from the a TF local-exec
 * provisioner to call back out into the gradle build to execute tasks that
 * do stuff that TF can't do.
 * <p/>
 * Yes, it'll be a nested stack of JVM -> TF -> JVM, you've got
 * plenty of RAM, right?
 */
class JavaCommand {
  TeregrinConfigurer config
  TeregrinLogger log

  private File javaHomeDir
  private File javaCommand


  void configureCommand() {
    if (javaCommand) {
      // already configured, skip
      return
    }

    String javaHomeString
    if (config.teregrin.javaHome) {
      log.debug "teregrin.javaHome set by user: ${teregrin.javaHome}"
      javaHomeString = new File(config.teregrin.javaHome)
    } else {
      String envVarHome = System.getenv('JAVA_HOME')
      if (envVarHome) {
        log.debug "teregrin.javaHome not set, using JAVA_HOME: $envVarHome"
        javaHomeString = envVarHome
      } else {
        String jreHomeDir = System.properties['java.home'] - 'jre'
        log.debug "teregrin.javaHome and JAVA_HOME not set, using homedir" +
          " of current JRE: ${jreHomeDir}"
        javaHomeString = jreHomeDir
      }
    }

    javaHomeDir = new File(javaHomeString)
    javaCommand = new File(javaHomeString, "bin/java")

  }
}

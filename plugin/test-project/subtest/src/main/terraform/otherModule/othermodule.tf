provider "aws" {
  region = "ap-southeast-2"
}

variable "vpc_cidr" {
  default = "10.86.0.0/16"
}

resource "aws_vpc" "teregrin-othermodule-vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags {
    Name = "teregrin-othermodule-vpc"
  }
}


resource "aws_security_group" "teregrin-othermodule-sg" {
  name = "teregrin-othermodule-sg"
  tags {
    Name = "teregrin-othermodule-sg"
  }
  description = "Security group used for testing teregrin"
  vpc_id = "${aws_vpc.teregrin-othermodule-vpc.id}"
}

//module "otherModule" {
//  source = "../otherModule"
//}

//module "devModule" {
//  source = "C:\\...\\teregrin\\teregrin-src\\plugin\\test-project\\src\\main\\terraform\\devPlatform"
//}

provider "aws" {
  region = "ap-southeast-2"
}

variable "vpc_cidr" {
  default = "10.85.0.0/16"
}

resource "aws_vpc" "teregrin-subtest-vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags {
    Name = "teregrin-subtest-vpc"
  }
}


resource "aws_security_group" "teregrin-subtest-sg" {
  name = "teregrin-subtest-sg"
  tags {
    Name = "teregrin-subtest-sg"
  }
  description = "Security group used for testing teregrin"
  vpc_id = "${aws_vpc.teregrin-subtest-vpc.id}"
}

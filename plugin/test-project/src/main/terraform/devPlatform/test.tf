provider "aws" {
  region = "ap-southeast-2"
}

variable "vpc_cidr" {
  default = "10.84.0.0/16"
}

resource "aws_vpc" "teregrin-test-vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags {
    Name = "teregrin-test-vpc"
  }
}


resource "aws_security_group" "teregrin-test-sg" {
  name = "teregrin-test-sg"
  tags {
    Name = "teregrin-test-sg"
  }
  description = "Security group used for testing teregrin"
  vpc_id = "${aws_vpc.teregrin-test-vpc.id}"
}
